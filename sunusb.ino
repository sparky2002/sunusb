/*
 * Arduino-based USB converter for Sun keyboard
 *
 * Copyright 2015 Alexis Parseghian
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/* WARNING
 *
 * This code uses the custom HID.cpp and USBAPI.h with rawPress() and
 * rawRelease() found here, not the ones from the Arduino core library.
 *
 */

/* data rate with host (when serial DEBUGging) and with keyboard */
#define HOST_SERIAL_RATE 9600	// comms with host (in debug mode)
#define KBD_SERIAL_RATE 1200	// comms with keyboard
#define DEBUG_PIN 12			// debug if pin grounded (jumpered)
#define WARM_UP 750				// wait for hardware to settle down (ms)
#define LED_MASK_WRITE 0x0e		// command prefix for sending LED mask to kbd
#define BELL_ON 0x02			// kbd commands to sound or stop beeping
#define BELL_OFF 0x03
#define CLICK_ON 0x0a			// kbd command to click on keypress
#define CLICK_OFF 0x0b

/* mask for LEDs */
const byte LED_NUMLK = B0001;
const byte LED_COMP  = B0010;
const byte LED_SCRLK = B0100;
const byte LED_CAPS  = B1000;

/* uninterpreted hardware key codes for modifiers */
const byte MOD_SCRLK = 23;
const byte MOD_NUMLK = 98;
const byte MOD_CAPS = 119;
const byte MOD_COMP = 67;
const byte MOD_LCTRL = 76; // Sun keyboards have only one Ctrl
const byte MOD_LSHFT = 99;
const byte MOD_RSHFT = 110;
const byte MOD_LALT = 19;
const byte MOD_LGUI = 120;
const byte MOD_RGUI = 122;
const byte MOD_RALT = 13;
const byte KP_EQUAL = 45; // aka Mute
const byte LINE_FEED = 111; // Not present on Type 5 ; used for internal config

/* key state toggle */
const byte KEYDOWN = 0;
const byte KEYUP = 1;

/* modifiers, USB value */
const byte ULCTRL = 224;
const byte ULSHFT = 225;
const byte ULALT = 226;
const byte ULGUI = 227;
const byte URSHFT = 229;
const byte URALT = 230;
const byte URGUI = 231;

/* Sun keys, USB value, as per USB HID Usage Table */

const byte SUNHELP = 117; 		// USB Help
const byte SUNSTOP = 120; 		// USB Stop
const byte SUNAGAIN = 121; 		// USB Again
const byte SUNPROPS = 163; 		// USB CrSel/Props
const byte SUNUNDO = 122; 		// USB Undo
const byte SUNFRONT = 119; 		// USB Select
const byte SUNCOPY = 124; 		// USB Copy
const byte SUNOPEN = 116; 		// USB Execute
const byte SUNPASTE = 125; 		// USB Paste
const byte SUNFIND = 126; 		// USB Find
const byte SUNCUT = 123; 		// USB Cut
// const byte SUNCOMPOSE = 118; 	// USB "Menu"
const byte SUNCOMPOSE = 101; 	// USB Application, works out of the box

/* Alternate table for Sun keys */
/*
const byte SUNSTOP = 104;		// Sun L1: F13
const byte SUNAGAIN = 105;		// Sun L2: F14
const byte SUNPROPS = 106;		// Sun L3: F15
const byte SUNUNDO = 107;		// Sun L4: F16
const byte SUNFRONT = 108;		// Sun L5: F17
const byte SUNCOPY = 109;		// Sun L6: F18
const byte SUNOPEN = 110;		// Sun L7: F19
const byte SUNPASTE = 111;		// Sun L8: F20
const byte SUNFIND = 112;		// Sun L9: F21
const byte SUNCUT = 113;		// Sun L10: F22
const byte SUNHELP = 115;		// F24
const byte SUNCOMPOSE = 114;	// F23
*/

/* Type 5 additional keys */
const byte SUNVOLDWN = 129;
const byte SUNVOLUP = 128;
const byte SUNMUTE = 127;
const byte SUNLUMDWN = 0; // TBD
const byte SUNLUMUP = 0;  // TBD
const byte SUNPOWER = 0;  // TBD

/* This adapter's own configuration keys (raw scan codes)*/
const byte KCNF_CLICK = 84; 	// K
const byte KCNF_COMPOSE = 102; 	// C
const byte KCNF_VERSION = 103;	// V

/* The following array is indexed on scan codes emitted from the keyboard,
 * and give out the expected USB HID keycode.
 * Drawn from the Sun Type 4 keyboard (with bits of the type 5).
 * Note: KP= on Type 4 is Mute on type 5.
 */
const byte keymap[] = {
	/* array offset */
	0,
	/* 1:Stop 2:undef 3:Again 4:VolUp 5:F1 6:F2 7:F10 8:F3 9:F11 10:F4 */
	SUNSTOP, 0, SUNAGAIN, 128, 58, 59, 67, 60, 68, 61,
	/* 11:F12 12:F5 13:ALTGR 14:F6 15:undef 16:F7 17:F8 18:F9 19:ALT 20:ARRUP */
	69, 62, URALT, 63, 0, 64, 65, 66, ULALT, 82,
	/* 21:PAUSE 22:PRSCR 23:SCRLK 24:ARRLF 25:PROPS 26:UNDO 27:ARRDN 28:ARRRG 29:ESC 30:1 */
	72, 70, 71, 80, SUNPROPS, SUNUNDO, 81, 79, 41, 30,
	/* 31:2 32:3 33:4 34:5 35:6 36:7 37:8 38:9 39:0 40:- */
	31, 32, 33, 34, 35, 36, 37, 38, 39, 45,
	/* 41:= 42:` 43:BACKSP 44:INS 45:MUTE 46:KP/ 47:KP* 48:PWR 49:FRONT(undef) 50:KP. */
	46, 53, 42, 73, SUNMUTE, 84, 85, 0, SUNFRONT, 99,
	/* 51: COPY 52:HOME 53:TAB 54:Q 55:W 56:E 57:R 58:T 59:Y 60:U */
	SUNCOPY, 74, 43, 20, 26, 8, 21, 23, 28, 24,
	/* 61:I 62:O 63:P 64:[ 65:] 66:DEL 67:COMP 68:KP7 69:KP8 70:KP9 */
	12, 18, 19, 47, 48, 76, SUNCOMPOSE, 95, 96, 97,
	/* 71:KP- 72:OPEN 73:PASTE 74:END 75:undef 76:LCTRL 77:A 78:S 79:D 80:F */
	86, SUNOPEN, SUNPASTE, 77, 0, ULCTRL, 4, 22, 7, 9,
	/* 81:G 82:H 83:J 84:K 85:L 86:; 87:' 88:\ 89:ENTER 90:KPENTER */
	10, 11, 13, 14, 15, 51, 52, 49, 40, 88,
	/* 91:KP4 92:KP5 93:KP6 94:KP0 95:FIND 96:PGUP 97:CUT 98:NUMLK 99:LSHIFT 100:Z */
	92, 93, 94, 98, SUNFIND, 75, SUNCUT, 83, ULSHFT, 29,
	/* 101:X 102:C 103:V 104:B 105:N 106:M 107:, 108:. 109:/ 110:RSHIFT */
	27, 6, 25, 5, 17, 16, 54, 55, 56, URSHFT,
	/* 111:LNFEED(undef) 112:KP1 113:KP2 114:KP3 115:undef 116:undef 117:undef 118:HELP 119:CAPS 120:LLOZ */
	0, 89, 90, 91, 0, 0, 0, SUNHELP, 57, ULGUI,
	/* 121:SPACE 122:RLOZ 123:PGDN 124:undef 125:KP+ 126:undef 127:undef 128:undef */
	44, URGUI, 78, 0, 87, 0, 0, 0
};

byte debug;					// use Serial if set. ONLY CHECKED AT SETUP.
byte incoming; 				// byte from keyboard
byte led_state = 0; 		// which leds are on
byte use_comp_led = 0;		// toggle Compose LED if set
byte key_state = KEYUP; 	// incoming key state (up/down)
byte shift_state = KEYUP; 	// Either Shift pressed
byte control_state = KEYUP; // Ctrl pressed
byte command_state = KEYUP; // Keyboard config key pressed

/* Fun bits */
byte bell_on;
byte click_on;

/*
 * switch keyboard LED on or off depending on led_state mask.
 * led argument is a single-bit mask against which led_state is tested; a
 * multi-bit (ORed) mask probably will be treated according to the first bit
 * found...
 */
void toggle_led(byte led) {
	if ((led_state & led) == led) {
		// already on, set off
		led_state &= ~led;
	}
	else {
		// set bit
		led_state |= led;
	}
	if (debug) {
		Serial.print("TGL ");
		Serial.print(led, BIN);
		Serial.print(" LEDS ");
		Serial.println(led_state, BIN);
	}
	Serial1.write(LED_MASK_WRITE);
	Serial1.write(led_state);
}

/*
 * keyboard beep for bt milliseconds (or stop if bt==0),
 * only if bell_on is set
 */
void set_bell(byte bt) {
	if (bt == 0) {
		Serial1.write(BELL_OFF);
	}
	else {
		if (bell_on) {
			Serial1.write(BELL_ON);
			delay(bt);
			Serial1.write(BELL_OFF);
		}
		else {
			Serial1.write(BELL_OFF);
		}
	}
}

/*
 * switch key click on or off
 */
void set_click(void) {
	if (click_on) {
		Serial1.write(CLICK_ON);
		set_bell(50);
	}
	else {
		Serial1.write(CLICK_OFF);
		set_bell(50);
		delay(50);
		set_bell(50);
	}
}

void toggle_click(void) {
	click_on = !click_on;
	set_click();
}

/*
 * switch shift or control internal flag on or off
 */
void set_mod_state(byte *state_holder, byte kstate) {
	*state_holder = kstate;
}

/*
 * blink LEDs in pattern to notify exiting setup.
 * This looks better on the Type 4 where the keys are side-by-side.
 */
void setup_complete_led() {
	byte pattern[] = { LED_NUMLK, LED_SCRLK, LED_COMP, LED_CAPS };
	int i;
	for (i=0; i<4; i++) {
		if (debug) {
			Serial.println(pattern[i]);
		}
		toggle_led(pattern[i]);
		delay(100);
		toggle_led(pattern[i]);
	}
	for (i=3; i>=0; i--) {
		toggle_led(pattern[i]);
		delay(100);
		toggle_led(pattern[i]);
	}
}

/*
 * Standard arduino init.
 *
 */
void setup() {

	delay(WARM_UP); // let the hardware settle down

	pinMode(DEBUG_PIN, INPUT_PULLUP);
	click_on = 1;
	bell_on = 1;

	// debug mode when pin is grounded (jumpered).
	// ONLY CHECK. DO NOT MODIFY WHEN IN USE.
	debug = (digitalRead(DEBUG_PIN) == LOW);

	// Serial/Keyboard faces the host computer
	if (debug) {
		Serial.begin(HOST_SERIAL_RATE);
	}
	else {
		Keyboard.begin();
	}

	// Serial1 interfaces with the keyboard
	Serial1.begin(KBD_SERIAL_RATE);
	set_bell(0);
	set_click();

	setup_complete_led();
}

/*
 * transient modifiers (Ctrl, Shift...) are sent via the regular
 * Keyboard.press() / release() so we reuse the existing bit-banging
 * on usb key report
 */
void send_modifier(int kcode, int kstate) {
	int sendval = 0;
	switch(kcode) {
		case MOD_LCTRL:
			sendval = KEY_LEFT_CTRL;
			break;
		case MOD_LSHFT:
			sendval = KEY_LEFT_SHIFT;
			break;
		case MOD_RSHFT:
			sendval = KEY_RIGHT_SHIFT;
			break;
		case MOD_LALT:
			sendval = KEY_LEFT_ALT;
			break;
		case MOD_LGUI:
			sendval = KEY_LEFT_GUI;
			break;
		case MOD_RGUI:
			sendval = KEY_RIGHT_GUI;
			break;
		case MOD_RALT:
			sendval = KEY_RIGHT_ALT;
			break;
	}
	if (! sendval == 0) {
		if (kstate == KEYDOWN) {
			Keyboard.press(sendval);
		}
		else {
			Keyboard.release(sendval);
		}
	}
}

/*
 * Standard arduino processing loop.
 * read keyboard, dispatch output to serial/usb
 */
void loop() {
	if (Serial1.available() > 0) {
		incoming = Serial1.read();
		if (incoming == 0x7f) {
			// keyboard sends 0x7f as idle mark when last key is released
			if (debug) {
				Serial.println("REL ALL");
			}
			else {
				Keyboard.releaseAll();
			}
		}
		else {
			// key press sends key code, release sends key code + 0x7f
			key_state = (incoming > 0x7f) ? KEYUP : KEYDOWN;
			incoming &= 0x7f;

			// transient modifiers are handled through the original,
			// ascii-interpreting code
			if ((incoming == MOD_LSHFT) || (incoming == MOD_RSHFT) ||
				(incoming == MOD_LCTRL) || (incoming == MOD_LALT) ||
				(incoming == MOD_LGUI) || (incoming == MOD_RGUI) ||
				(incoming == MOD_RALT) || (incoming == LINE_FEED))
			{
				if ((incoming == MOD_LSHFT) || (incoming == MOD_RSHFT)) {
					set_mod_state(&shift_state, key_state);
				}
				else if (incoming == MOD_LCTRL) {
					set_mod_state(&control_state, key_state);
				}
				else if (incoming == LINE_FEED) {
					set_mod_state(&command_state, key_state);
					toggle_led(LED_SCRLK);
					toggle_led(LED_CAPS);
				}
				if (!debug) {
					send_modifier(incoming, key_state);
				}
			}

			if (key_state == KEYUP) {
				switch(incoming) {
					// modifier toggles are only considered on release wrt LEDs
					case MOD_CAPS:
						toggle_led(LED_CAPS);
						break;
					case MOD_COMP:
						if (use_comp_led) {
							toggle_led(LED_COMP);
						}
						break;
					case MOD_NUMLK:
						toggle_led(LED_NUMLK);
						break;
					case MOD_SCRLK:
						toggle_led(LED_SCRLK);
						break;

					/* Special handling: local configuration */
					case KCNF_CLICK:
						// toggle key click
						if (command_state == KEYDOWN) {
							toggle_click();
						}
						break;
					case KCNF_COMPOSE:
						// toggle usage of Compose LED when Compose key pressed
						if (command_state == KEYDOWN) {
							use_comp_led = !use_comp_led;
						}
						break;

				}
				if (debug) {
					Serial.print("KUP ");
					Serial.print(incoming);
					Serial.print(" ");
					Serial.println(keymap[incoming]);
				}
				else {
					Keyboard.rawRelease(keymap[incoming]);
				}
			}
			else {
				if (debug) {
					Serial.print("KDN ");
					Serial.print(incoming);
					Serial.print(" ");
					Serial.println(keymap[incoming]);
				}
				else {
					// Keyboard.rawPress(incoming);
					Keyboard.rawPress(keymap[incoming]);
				}
			}
		}
	}
}
